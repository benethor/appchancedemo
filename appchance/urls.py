# coding=utf-8

from django.contrib import admin
from django.urls import include, path

from appchance.demoapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin/django-rq/', include('django_rq.urls')),
    path('', views.MainView, name='mainform'),
    path('results/<str:job_uuid>/', views.ResultsView, name='results'),

]
