# coding=utf-8

import hashlib


def get_cache_key(query):
    """Generates the cache key for results storage."""

    hasher = hashlib.sha256()
    hasher.update(query.encode('utf-8', errors='backslashreplace'))

    return "results-{}".format(hasher.hexdigest())
