# coding=utf-8
import json
import logging
import re
import requests
from bs4 import BeautifulSoup
from django_rq import job
from django.conf import settings
from django.core.cache import cache
from requests.exceptions import RequestException
from urllib.parse import quote_plus, urlparse, parse_qs

from appchance.demoapp import choices
from appchance.demoapp.helpers import get_cache_key
from appchance.demoapp.models import SearchStatsTask

USER_AGENT = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)'
URL_HOME = 'https://www.google.com'
URL_SEARCH = "https://www.google.com/search?hl=en&q={query}&btnG=Google+Search&tbs=0&safe=off"
DEFAULT_HEADERS = {
    'User-Agent': USER_AGENT
}

log = logging.getLogger(__name__)


def save_results(query, data):
    '''Dumps the results to cache.'''

    cache_key = get_cache_key(query)
    cache.set(cache_key, json.dumps(data), settings.RESULT_CACHE_TIME)


def clean_link(link):
    """Cleans the link passed from Google results page."""
    try:
        o = urlparse(link, 'http')
        if o.netloc and 'google' not in o.netloc:
            return link

        if link.startswith('/url?'):
            link = parse_qs(o.query)['q'][0]

            o = urlparse(link, 'http')
            if o.netloc and 'google' not in o.netloc:
                return link

    except Exception as ex:
        log.warning(
            'Something weird has happened while cleaning %s. '
            'The Exception was: %s', link, ex
        )
        pass
    return None


@job
def process_query(task_uuid):
    try:
        instance = SearchStatsTask.objects.get(id=task_uuid)
    except SearchStatsTask.DoesNotExist:
        log.error('Could not find task with id=%s', task_uuid)
        return

    result = {'status': 'ok', 'query': instance.query}
    try:
        s = requests.Session()
        s.get(URL_HOME, headers=DEFAULT_HEADERS)  # nom default cookies

        url = URL_SEARCH.format(query=quote_plus(instance.query))

        response = s.get(url, headers=DEFAULT_HEADERS)
    except RequestException:
        instance.status = choices.TASKSTATE_ERROR
        instance.save(update_fields=['status'])

    dom = BeautifulSoup(response.text, 'lxml')

    # find the hit count
    hits_tag = dom.find_all(attrs={"class": "sd", "id": "resultStats"})[0]
    hits_text_parts = hits_tag.text.split()
    hits = int(hits_text_parts[1].replace(',', '').replace('.', ''))
    result['hits'] = hits

    # save the search results
    search_results = dom.find(id='search').find_all('div', {'class': 'g'})
    pos = 1
    result['searchresults'] = []
    for entry in search_results:
        out = {}
        link = entry.find('a')
        if not link or not link.parent or link.parent.name.lower() != "h3":
            # not a "standard" link, probably some embed, or a FAQ
            continue

        out['pos'] = pos
        out['url'] = clean_link(link['href'])
        if not out['url']:  # imgaes
            continue

        out['title'] = entry.find('h3').text
        out['description'] = entry.find(
            'span', {'class': 'st'}
        ).get_text().replace('\n', '')

        result['searchresults'].append(out)
        pos += 1

    # generate the map of most popular words
    wordmap = dict()
    words = ' '.join(
        [x['title'] + x['description'] for x in result['searchresults']]
    )
    for word in re.sub('[0-9\W]+', ' ', words).split(' '):  # W605 here is f/p
        if len(word) < 3:
            # let's skip the meaningless parts
            continue

        entry = word.lower()
        if entry in wordmap:
            wordmap[entry] += 1
        else:
            wordmap[entry] = 1

    result['topwords'] = sorted(wordmap.items(), key=lambda x: x[1], reverse=True)[:10]  # noqa

    save_results(instance.query, result)
    instance.status = choices.TASKSTATE_OK
    instance.save(update_fields=['status'])

    return True
