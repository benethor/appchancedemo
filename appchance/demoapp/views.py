# coding=utf-8
import json
from django.core.cache import cache
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from appchance.demoapp import choices
from appchance.demoapp.forms import QueryForm
from appchance.demoapp.helpers import get_cache_key
from appchance.demoapp.models import SearchStatsTask
from appchance.demoapp.tasks import process_query


def MainView(request):
    form = QueryForm(request.POST)
    sourceip = request.META['REMOTE_ADDR']
    if request.method == 'POST':
        if form.is_valid():
            try:
                task = SearchStatsTask.objects.get(
                    query=form.cleaned_data['query'], sourceip=sourceip
                )
            except SearchStatsTask.DoesNotExist:
                # I don't really trust get_or_create(), failed me too many times
                task = SearchStatsTask.objects.create(
                    query=form.cleaned_data['query'], sourceip=sourceip,
                    status=choices.TASKSTATE_NEW
                )

            return HttpResponseRedirect(
                reverse('results', args=[task.pk])
            )
        else:
            context = {
                'invalid_url': True,
                'form': form
            }
            return render(request, 'inputform.html', context)

    return render(request, 'inputform.html')


def ResultsView(request, job_uuid):
    try:
        instance = SearchStatsTask.objects.get(pk=job_uuid)
        context = {
            'job_status': instance.status,
            'query': instance.query,
        }

        cache_key = get_cache_key(instance.query)
        results = cache.get(cache_key)
        if results:
            context['results'] = json.loads(results)
        else:
            # run the job if there's not results in cache
            if (instance.status != choices.TASKSTATE_INPROG) and not results:
                process_query.delay(task_uuid=instance.pk)
                instance.status = choices.TASKSTATE_INPROG
                instance.save(update_fields=['status'])

    except (SearchStatsTask.DoesNotExist, ValueError):
        context = {'does_not_exist': True}

    return render(request, 'results.html', context)
