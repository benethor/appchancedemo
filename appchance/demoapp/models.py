# coding=utf-8
import uuid
from django.db import models

from appchance.demoapp import choices


class SearchStatsTask(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    query = models.CharField(
        null=False, blank=False, max_length=255
    )
    created = models.DateTimeField(auto_now_add=True)
    sourceip = models.GenericIPAddressField(
        null=True, blank=True, protocol="IPv4"
    )
    status = models.PositiveSmallIntegerField(
        null=False, blank=False, choices=choices.TASKSTATES,
        default=choices.TASKSTATE_NEW
    )

    class Meta:
        verbose_name = "SearchStats task"
        verbose_name_plural = "SearchStats tasks"
        ordering = ['-id']

    def __str__(self):
        return self.query
