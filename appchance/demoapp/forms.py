# coding=utf-8
from django import forms


class QueryForm(forms.Form):
    query = forms.CharField(label='Search query to process', max_length=255)
