# coding=utf-8
from django.test import Client, TestCase
from django.urls import reverse

from appchance.demoapp import choices
from appchance.demoapp.models import SearchStatsTask


class MainViewTests(TestCase):
    '''Tests for the main view, aka the input form'''

    def setUp(self):
        self.client = Client()

    def tearDown(self):
        SearchStatsTask.objects.all().delete()

    def test_should_render_page(self):
        response = self.client.get(reverse('mainform'))

        self.assertEqual(response.status_code, 200)

    def test_should_render_inputform(self):
        response = self.client.get(reverse('mainform'))
        template_name = response.templates[0].name

        self.assertEqual(template_name, 'inputform.html')

    def test_should_redirect_on_valid_url(self):
        response = self.client.post(
            reverse('mainform'), {'query': 'test'}
        )
        expected = reverse('results', args=[SearchStatsTask.objects.first().pk])

        self.assertRedirects(response, expected)

    def test_should_create_task(self):
        '''Test whether task objects are created correctly'''
        target = 'test'

        before = SearchStatsTask.objects.count()
        self.client.post(reverse('mainform'), {'query': target})
        after = SearchStatsTask.objects.count()
        instance = SearchStatsTask.objects.get(query=target)

        self.assertTrue(after > before)
        self.assertEqual(instance.status, choices.TASKSTATE_NEW)

    def test_should_only_create_one(self):
        '''Only one task per target should be created'''
        target = 'test'

        start = SearchStatsTask.objects.count()
        self.client.post(reverse('mainform'), {'query': target})
        after_first = SearchStatsTask.objects.count()
        self.client.post(reverse('mainform'), {'query': target})
        after_second = SearchStatsTask.objects.count()

        self.assertTrue(after_first > start)
        self.assertEqual(after_first, after_second)
