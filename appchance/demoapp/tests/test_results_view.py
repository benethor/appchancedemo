# coding=utf-8
import json
from django.core.cache import cache
from django.test import Client, TestCase
from django.urls import reverse
from mock import patch

from appchance.demoapp import choices
from appchance.demoapp.helpers import get_cache_key
from appchance.demoapp.models import SearchStatsTask


SAMPLE_RESULTS = {
    'status': 'ok',
}


class ResultsViewTests(TestCase):
    '''Tests for the results view'''

    def setUp(self):
        self.client = Client()
        self.task = SearchStatsTask.objects.create(
            query='test', status=choices.TASKSTATE_INPROG
        )

    def tearDown(self):
        SearchStatsTask.objects.all().delete()

    def test_should_render_template(self):
        response = self.client.get(reverse('results', args=[self.task.pk]))
        template_name = response.templates[0].name

        self.assertEqual(template_name, 'results.html')

    def test_should_survive_an_error(self):
        response = self.client.get(
            reverse('results', args=['00000000-0000-0000-0000-000000000000'])
        )

        self.assertEqual(response.status_code, 200)

    @patch(
        'appchance.demoapp.views.cache.get',
        return_value=json.dumps(SAMPLE_RESULTS)
    )
    def test_should_attempt_cache_read(self, mocked):
        self.client.get(reverse('results', args=[self.task.pk]))

        self.assertTrue(mocked.called)

    @patch('appchance.demoapp.views.process_query.delay')
    def test_should_create_rq_job_for_new_tasks(self, mocked):
        self.task.status = choices.TASKSTATE_NEW
        self.task.save()

        self.client.get(reverse('results', args=[self.task.pk]))

        mocked.assert_called_with(task_uuid=self.task.pk)

    @patch('appchance.demoapp.views.process_query.delay')
    def test_should_not_create_jobs_on_cache_hit(self, mocked):
        self.task.status = choices.TASKSTATE_NEW
        self.task.save()
        cache_key = get_cache_key(self.task.query)

        cache.set(cache_key, json.dumps(SAMPLE_RESULTS))
        self.client.get(reverse('results', args=[self.task.pk]))

        mocked.assert_not_called()
