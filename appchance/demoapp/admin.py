# coding=utf-8
from django.contrib import admin

from appchance.demoapp import models


class SearchStatsTaskAdmin(admin.ModelAdmin):
    search_fields = ['query', 'sourceip']
    list_display = ['query', 'created', 'status', 'sourceip']
    list_filter = ['status']


admin.site.register(models.SearchStatsTask, SearchStatsTaskAdmin)
