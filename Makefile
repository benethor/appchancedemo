install: install-local-reqs
	pip install -e .

install-local-reqs:
	pip install -r requirements/local.txt

install-deploy-reqs:
	pip install -r requirements/deploy.txt

nose:
	DJANGO_SETTINGS_MODULE=appchance.settings.testing DJANGO_TEST_RUNNER=nose \
	NOSE_NOLOGCAPTURE=true NOSE_NOCAPTURE=true NOSE_WITH_XUNIT=true \
	coverage run --source=appchance \
	--omit='*migrations*,*tests*,*__init__*,*__main__*,*wsgi.py,*settings*,*manage.py,*admin.py' \
	appchance test appchance --verbosity=1

flake8:
	flake8 --ignore=E501,F405,W605 --exclude=migrations,settings appchance

coverage: nose
	coverage report -m

tests: flake8 coverage

run:
	appchance runserver 0.0.0.0:8000

worker:
	appchance rqworker default
