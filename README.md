# Appchance-demo

Appchance recrutation demo service. Version: 0.1.0

## Usage

This is just a sample to show off a little. It's not meant to run in production, so everything should be launched via vagrant.

Commands:

 * `make run` - runs the frontend at localhost:8000
 * `make worker` - runs the background tasks worker
 * `make tests` - runs some basic unit tests, linter and coverage checks
