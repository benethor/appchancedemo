#!/usr/bin/env bash

set -e

# Fetch latest package lists
sudo apt-get update

# Install essential packages
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install \
    build-essential \
    python-virtualenv \
    python3-dev \
    git \
    libldap2-dev \
    libsasl2-dev \
    libsqlite3-dev \
    redis-server \
    mc \
    vim

# Prepare virtualenv and chdir to application's dir
virtualenv --python=/usr/bin/python3 --no-site-packages .
. bin/activate
cd /home/vagrant/src/appchance

# Install the application
make install
make install-deploy-reqs

# Install initial migrations set and collect static files
export DJANGO_SETTINGS_MODULE=appchance.settings.local
export PROJECT_PATH=/home/vagrant/src/appchance
appchance migrate --noinput
appchance collectstatic --noinput

# Initialize virtualenv at every login (and set up some necessary stuff)
cat >> /home/vagrant/.profile << EOF
source /home/vagrant/bin/activate
export DJANGO_SETTINGS_MODULE=appchance.settings.local
export PROJECT_PATH=/home/vagrant/src/appchance
cd src/appchance
echo "To start an instance of your project, enter \"make run\" from this dir."
echo "To start worker process, open another console and enter \"make worker\" (also from this dir)."
echo "To to run the tests package, use \"make tests\"."
EOF

echo
echo "Your development VM has been successfully provisioned. You can start using it with \"vagrant ssh\"."
echo
